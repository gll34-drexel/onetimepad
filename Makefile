compile: encrypt.o decrypt.o
encrypt: in_file encrypt.o
	./encrypt.o in_file pad cyph
decrypt: cyph pad decrypt.o
	./decrypt.o cyph pad out_file
clean:
	rm -fv cyph pad 
secure: encrypt 
	rm -fv in_file 
plain: decrypt clean

encrypt.o: encrypt.c
	gcc -Wall -Wextra encrypt.c -o encrypt.o -pthread
decrypt.o: decrypt.c
	gcc -Wall -Wextra decrypt.c -o decrypt.o -pthread
