# One-time pad encryption 
By George Lippincott
## Description
Implements one-time pad encryption on a file, by generating a ciphertext and pad of equal length.

## How it Works
A string of random bytes equal to the length of the input file is generated. Then, these bytes are XOR'd with the input file bytes, producing the ciphertext.
Decryption is simply taking the XOR of the bytes in the pad file and ciphertext file.

## Requirements
The Gnu C Compiler, GNU make, as well as access to the standard /dev/urandom (usually requires \*nix, Mac OS, or similar). Windows support coming soon.

## Usage
### Manual mode
**./encrypt.o [in_file] [pad] [cyph]** will encrypt "in_file" and write to the files "pad" and "cyph"

**./decrypt.o [cyph] [pad] [out_file]** will decrypt "pad" and "cyph" via XOR and write to the file "out_file"
### Auto mode
To encrypt, run **make encrypt**, which requires the file to be named "in_file"

To decrypt, run **make decrypt**, which requires the encrypted files to be named "cyph" and the one-time pad "pad"

**make secure**- encrypts and deletes "in_file"

**make plain**- decrypts and deletes "pad" and "cyph"
