#include <stdio.h>
#include <assert.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#include <pthread.h>
#include "status.h"
#define BSIZE 4096
//One time pad in C


int main(int argc, char **argv)
{
    if (argc < 4) {
	puts("provide a filename for input, pad, and cypher");
	exit(1);
    }
    pthread_t status_thread;
    char *buf = malloc(sizeof(char) * BSIZE);
    char *rbuf = malloc(sizeof(char) * BSIZE);
    char *proc_buf = malloc(sizeof(char) * BSIZE);
    FILE *test = fopen(argv[1], "r");
    FILE *rand = fopen("/dev/urandom", "r");
    FILE *cyp = fopen(argv[3], "w");
    FILE *pad = fopen(argv[2], "w");
    fseek(test, 0L, SEEK_END);
    long sz = ftell(test);
    rewind(test);
	//create status thread
	struct threadthing stats;
	stats.sz = &sz;
	stats.fl = &test;
    printf("file size is %ld bytes\n", sz);
    puts("encrypting...");
    time_t start = time(NULL);
    size_t b_read = 0L;
	if(pthread_create(&status_thread, NULL, status_bar, &stats)){
	fprintf(stderr, "thread not created\n");
	exit(1);
	}
    while ((b_read = fread(buf, sizeof(char), BSIZE, test)) > 0) {
	size_t tmprd = fread(rbuf, sizeof(char), b_read, rand);
	assert(tmprd == b_read);
	for (unsigned int i = 0; i < b_read; ++i) {
	    proc_buf[i] = buf[i] ^ rbuf[i];
	}
	fwrite(rbuf, sizeof(char), b_read, pad);
	fwrite(proc_buf, sizeof(char), b_read, cyp);
    }
    pthread_join(status_thread, NULL);
    fclose(test);
    fclose(rand);
    fclose(cyp);
    fclose(pad);
    free(buf);
    free(rbuf);
    free(proc_buf);
    time_t end = time(NULL);
    printf("wrote to %s, %s\n", argv[2], argv[3]);
    printf("encryption complete in %lu seconds\n", (long) (end - start));
    return 0;
}

