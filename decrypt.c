#include <stdio.h>
#include <assert.h>
#include <stdlib.h>
#include <time.h>
#define BSIZE 4096

int main(int argc, char **argv)
{
    if (argc < 4) {
	puts("please provide ciph, key, output filenames");
    }
    FILE *cyp = fopen(argv[1], "r");
    FILE *pd = fopen(argv[2], "r");
    FILE *clr_out = fopen(argv[3], "w");
    char *cb = malloc(sizeof(char) * BSIZE);
    char *pdb = malloc(sizeof(char) * BSIZE);
    char *pbuf = malloc(sizeof(char) * BSIZE);
    size_t b_read = 0L;
    time_t start = time(NULL);
    while ((b_read = (fread(cb, sizeof(char), BSIZE, cyp))) > 0) {
	size_t tmprd = fread(pdb, sizeof(char), b_read, pd);
	assert(tmprd == b_read);
	for (unsigned int i = 0; i < b_read; ++i) {
	    pbuf[i] = cb[i] ^ pdb[i];
	}
	fwrite(pbuf, sizeof(char), b_read, clr_out);
    }


    time_t end = time(NULL);
    free(cb);
    free(pdb);
    free(pbuf);
    fclose(cyp);
    fclose(pd);
    fclose(clr_out);
    printf("wrote to %s\n", argv[3]);
    printf("decryption complete in %lu seconds\n", (long) (end - start));
    return 0;
}
