
struct threadthing {
	long* sz;
	FILE** fl;
};


void * status_bar (void* struct_ptr){
	struct threadthing* strct = struct_ptr;
	long size = *(strct->sz);
	FILE* filep = *(strct->fl);
	while( ftell(filep) != size){
	printf("\r");
	printf("%0.2f%%", 100.0 * ((double) ftell(filep) )/ size);
	fflush(stdout);
	sleep(1);
	}
	puts("\r100.00%");
	return NULL;
}

